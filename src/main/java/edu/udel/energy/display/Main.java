package edu.udel.energy.display;

import com.google.common.base.Stopwatch;
import edu.udel.energy.display.util.BufferedImageUtils;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import joptsimple.util.PathConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.util.streamex.StreamEx;
import java.awt.image.BufferedImage;
import java.awt.image.LookupOp;
import java.awt.image.LookupTable;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static joptsimple.util.PathProperties.DIRECTORY_EXISTING;
import static joptsimple.util.PathProperties.READABLE;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException {

        OptionParser parser = new OptionParser();

        OptionSpec<Display> displaySpec = parser.accepts("display")
                .withRequiredArg()
                .ofType(Display.class)
                .defaultsTo(Display.OLED);

        OptionSpec<Path> outdirSpec = parser.accepts("output")
                .withRequiredArg()
                .withValuesConvertedBy(new PathConverter());

        OptionSpec<String> pathMatcherPattern = parser.accepts("screenshot-pattern")
                .withRequiredArg()
                .ofType(String.class)
                .defaultsTo("glob:**.{png}")
                .describedAs("syntax and pattern used to filter files in the screen shot directories.");

        OptionSpec<String> timestampPatternSpec = parser.accepts("timestamp-regex")
                .withRequiredArg()
                .ofType(String.class)
                .defaultsTo("screenshot_(?<timestamp>\\d+).png")
                .describedAs("regular expression used to extract the timestamp from the screenshot filename.  The regex must indicate the timestamp using a named capture group.");

        OptionSpec<Void> helpSpec = parser.accepts("help", "print help").forHelp();

        OptionSpec<Path> screenshotDirSpec = parser.nonOptions("screenshot directories")
                .withValuesConvertedBy(new PathConverter(DIRECTORY_EXISTING, READABLE));

        OptionSet options = parser.parse(args);

        if (options.has(helpSpec) || !options.hasArgument(screenshotDirSpec)) {
            parser.printHelpOn(System.err);
            System.exit(0);
        }

        Display display = options.valueOf(displaySpec);
        Pattern timestampPattern = Pattern.compile(options.valueOf(timestampPatternSpec));
        PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher(options.valueOf(pathMatcherPattern));

        for (Path screenshotDir : options.valuesOf(screenshotDirSpec)) {

            logger.debug("Start directory: {}", screenshotDir);

            Stopwatch stopwatch = Stopwatch.createStarted();

            List<Map.Entry<Path, Long>> screenshots = StreamEx.of(Files.list(screenshotDir))
                    .filter(pathMatcher::matches)
                    .mapToEntry(path -> {
                        Matcher matcher = timestampPattern.matcher(path.getFileName().toString());
                        matcher.matches();
                        return Long.parseLong(matcher.group("timestamp"));
                    })
                    .sorted(Map.Entry.comparingByValue())
                    .toList();

            double sourceEnergy = 0;
            double alternateEnergy = 0;

            for (int index = screenshots.size() - 2; index >= 0; index--) {
                Map.Entry<Path, Long> current = screenshots.get(index);
                Path path = current.getKey();

                logger.debug("Start screenshot: {}", path);

                Map.Entry<Path, Long> subsequent = screenshots.get(index + 1);

                long duration = subsequent.getValue() - current.getValue();

                BufferedImage oldImage = ImageIO.read(path.toFile());

                Map<Integer, Long> oldHistogram = BufferedImageUtils.histogram(oldImage);

                Map<Integer, Long> newHistogram = oldHistogram.entrySet().stream()
                        .collect(Collectors.toMap(
                                e -> display.optimizedPixel(e.getKey()),
                                Map.Entry::getValue,
                                (v1, v2) -> v1 + v2));

                sourceEnergy += display.power(oldHistogram) * duration;
                alternateEnergy += display.power(newHistogram) * duration;

                if (options.has(outdirSpec)) {
                    LookupOp imageOptimizer = new LookupOp(new LookupTable(0, 3) {
                        @Override
                        public int[] lookupPixel(int[] srcRGB, int[] destRGB) {
                            if (null == destRGB) {
                                destRGB = new int[srcRGB.length];
                            }

                            int[] optimizedRGB = display.optimizedRGB(srcRGB);
                            System.arraycopy(optimizedRGB, 0, destRGB, 0, optimizedRGB.length);
                            return destRGB;
                        }
                    }, null);

                    BufferedImage newImage = imageOptimizer.filter(oldImage, null);
                    Path outPath = options.valueOf(outdirSpec).resolve(path);
                    Files.createDirectories(outPath.getParent());
                    ImageIO.write(newImage, "png", outPath.toFile());
                }

                logger.debug("Finish screenshot: {} (elapsed {})", path, stopwatch);
            }

            logger.debug("Finish directory : {} (elapsed {})", screenshotDir, stopwatch);

            double percentChange = ((alternateEnergy - sourceEnergy) / sourceEnergy) * 100;

            System.out.printf("%s - original: %.2f mj, optimized: %.2f mj, change: %.2f%%\n",
                    screenshotDir, sourceEnergy, alternateEnergy, percentChange);
        }
    }
}


