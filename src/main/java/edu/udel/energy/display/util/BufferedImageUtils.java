package edu.udel.energy.display.util;

import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.PixelGrabber;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class BufferedImageUtils {

    public static Map<Integer, Long> histogram(final BufferedImage image) {
        return pixels(image)
                .collect(Collectors.groupingBy(
                        Function.identity(),
                        Collectors.counting()));
    }

    public static Stream<Integer> pixels(final BufferedImage image) {
        int w = image.getWidth();
        int h = image.getHeight();

        int[] pixels = new int[w * h];
        try {
            PixelGrabber pg = new PixelGrabber(image, 0, 0, w, h, pixels, 0, w);
            pg.grabPixels();
            if ((pg.getStatus() & ImageObserver.ABORT) != 0) {
                throw new RuntimeException("failed to load image contents");
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("image load interrupted");
        }

        return Arrays.stream(pixels)
                // drop alpha component
                .map(i -> i & 0xffffff)
                .boxed();
    }
}
