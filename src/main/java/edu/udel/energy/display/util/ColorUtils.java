package edu.udel.energy.display.util;

import com.google.common.base.Preconditions;

public class ColorUtils {

    public static int red(int pixel) {
        return (pixel >> 16) & 0xff;
    }

    public static int green(int pixel) {
        return (pixel >> 8) & 0xff;
    }

    public static int blue(int pixel) {
        return pixel & 0xff;
    }

    public static int[] pixel2rgb(int pixel) {
        int r = red(pixel);
        int g = green(pixel);
        int b = blue(pixel);
        return new int[]{r, g, b};
    }

    public static int rgb2pixel(int[] rgb) {
        Preconditions.checkArgument(3 == rgb.length);
        return rgb2pixel(rgb[0], rgb[1], rgb[2]);
    }

    public static int rgb2pixel(int r, int g, int b) {
        return ((r & 0x0ff) << 16) | ((g & 0x0ff) << 8) | (b & 0x0ff);
    }

}
