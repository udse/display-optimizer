package edu.udel.energy.display;

import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Range;
import com.google.common.primitives.Ints;
import edu.udel.energy.display.util.BufferedImageUtils;
import edu.udel.energy.display.util.ColorUtils;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.util.streamex.StreamEx;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import static edu.udel.energy.display.CIEDE2000.deltaE2000;
import static edu.udel.energy.display.ColorSpaceConverter.RGBtoLAB;

public enum Display {

    S2(0.0001499335, 0.000155904, 0.0002210361, 0.00013333613, 0.00014338309, 0.0001393543),
    NEXUS(5.2051e-9, 6.3110e-9, 7.1614e-9, 1.2270e-4, 1.2566e-4, 1.2336e-4),
    OLED(0.0138118, 0.0059778, 0.005288, 0.00152877, 0.00174489, 0.00154593);

    private static final Logger logger = LoggerFactory.getLogger(Display.class);

    private static final int OFFSET = 30;
    private static final double THRESHOLD = 2.3;

    private final double f;
    private final double g;
    private final double h;
    private final double fudge;
    private final ConcurrentMap<Integer, Integer> optimizedColorMap;

    Display(final double f, final double g, final double h, final double fc, final double gc, final double hc) {
        this.f = f;
        this.g = g;
        this.h = h;
        this.fudge = fc + gc + hc;

        this.optimizedColorMap = DBMaker.fileDB(String.format("%s.db", this.name()))
                .fileMmapEnable()
                .closeOnJvmShutdown()
                .make()
                .hashMap("map", Serializer.INTEGER, Serializer.INTEGER)
                .createOrOpen();
    }

    public double power(final Map<Integer, Long> histogram) {
        return histogram.entrySet().stream()
                .collect(Collectors.summingDouble(e -> power(e.getKey()) * e.getValue()));
    }

    public double power(final BufferedImage image) {
        return power(BufferedImageUtils.histogram(image));
    }

    public double power(final int r, final int g, final int b) {
        double red = linearize(r) * this.f;
        double green = linearize(g) * this.g;
        double blue = linearize(b) * this.h;

        return (red + green + blue) + this.fudge;
    }

    public double power(final int[] rgb) {
        return power(rgb[0], rgb[1], rgb[2]);
    }

    public double power(final int pixel) {
        return power(ColorUtils.pixel2rgb(pixel));
    }

    private static double linearize(double value) {
        double linear = value / 255;
        if (linear <= 0.04045) {
            linear = linear / 12.92;
        } else {
            linear = Math.pow((linear + 0.055) / (1 + 0.055), 2.4);
        }
        return linear;
    }

    public int optimizedPixel(int pixel) {
        return optimizedColorMap.computeIfAbsent(pixel, this::findOptimizedColor);
    }

    public int[] optimizedRGB(int[] rgb) {
        return ColorUtils.pixel2rgb(optimizedPixel(ColorUtils.rgb2pixel(rgb)));
    }

    private int findOptimizedColor(int pixel) {

        logger.warn("Calculating optimized color for {}", Arrays.toString(ColorUtils.pixel2rgb(pixel)));

        int r = ColorUtils.red(pixel);
        int g = ColorUtils.green(pixel);
        int b = ColorUtils.blue(pixel);

        int minR = Math.max(0, r - OFFSET);
        int maxR = Math.min(255, r + OFFSET);
        Range<Integer> redRange = Range.closed(minR, maxR);

        int minG = Math.max(0, g - OFFSET);
        int maxG = Math.min(255, g + OFFSET);
        Range<Integer> greenRange = Range.closed(minG, maxG);

        int minB = Math.max(0, b - OFFSET);
        int maxB = Math.min(255, b + OFFSET);
        Range<Integer> blueRange = Range.closed(minB, maxB);

        Set<Integer> reds = ContiguousSet.create(redRange, DiscreteDomain.integers());
        Set<Integer> greens = ContiguousSet.create(greenRange, DiscreteDomain.integers());
        Set<Integer> blues = ContiguousSet.create(blueRange, DiscreteDomain.integers());

        double[] source_lab = RGBtoLAB(r, g, b);

        return StreamEx.cartesianProduct(ImmutableList.of(reds, greens, blues))
                .parallel()
                .map(Ints::toArray)
                .filter(rgb -> deltaE2000(source_lab, RGBtoLAB(rgb)) <= THRESHOLD)
                .min(Comparator.comparingDouble(this::power))
                .map(ColorUtils::rgb2pixel)
                .orElse(pixel);
    }
}
